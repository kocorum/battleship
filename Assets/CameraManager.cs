using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// At the moment, the camera manager queues up every movement requested, not giving the option to cancel them. 
/// </summary>
public class CameraManager : MonoBehaviour
{
    public Camera cam;

	private List<Action> MovementQueue { get; set; } = new List<Action>();
    public bool IsMoving { get; private set; } = false;


	protected float ZDistanceThatFitsObject(GameObject obj, float factor = 1f)
	{
		return ZDistanceThatFitsBounds(GetTotalBounds(obj), factor);
	}
	protected float ZDistanceThatFitsBounds(UnityEngine.Bounds bounds, float factor = 1f)
	{
		Vector3 objectSizes = bounds.max - bounds.min;
		float objectSize = Mathf.Max(objectSizes.x, objectSizes.y, objectSizes.z);
		float cameraView = 2.0f * Mathf.Tan(0.5f * Mathf.Deg2Rad * cam.fieldOfView); // Visible height 1 meter in front
		float distance = factor * objectSize / cameraView; // Combined wanted distance from the object
		distance += 0.5f * objectSize; // Estimated offset from the center to the outside of the object
		//cam.transform.position = totalBounds.center - distance * cam.transform.forward;


		// maybe this aint good. previous line works but also aligns with the object, which we don't necessarily want to do here.
		return bounds.center.z - (distance * cam.transform.forward.z);
		//cam.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y, totalBounds.center.z - (distance * cam.transform.forward.z));
	}
	protected UnityEngine.Bounds GetTotalBounds(params GameObject[] objs)
	{
		UnityEngine.Bounds totalBounds = new UnityEngine.Bounds();
		bool initialized = false;

		foreach (var obj in objs)
		{
			foreach (var renderer in obj.GetComponentsInChildren<Renderer>().Concat(obj.GetComponents<Renderer>()).Where(z => z != null))
			{
				if (!initialized)
				{
					totalBounds = renderer.bounds;
					initialized = true;
				}
				else
					totalBounds.Encapsulate(renderer.bounds);
			}
		}
		return totalBounds;
	}

	public void MoveTo(Vector3 position, float duration, Action endCallback)
	{
		Action action = () =>
		{
			if (duration == 0f || cam.transform.position == position)
			{
				cam.transform.position = position;
				IsMoving = false;
				endCallback?.Invoke();
			}
			else
			{
				StartCoroutine(MoveFromTo(cam.transform.position, position, duration, endCallback));
			}
		};

		MovementQueue.Add(action);
		StartNextMovement();
	}
	private IEnumerator MoveFromTo(Vector3 pos1, Vector3 pos2, float duration, Action endCallback = null)
	{
		for (float t = 0f; t < duration; t += Time.deltaTime)
		{
			IsMoving = true;
			cam.transform.position = Vector3.Lerp(pos1, pos2, t / duration);
			yield return 0;
		}
		cam.transform.position = pos2;
		IsMoving = false;
		endCallback?.Invoke();
	}

	private void StartNextMovement()
	{
		if (!IsMoving && MovementQueue.Any())
		{
			MovementQueue.First().Invoke();
			MovementQueue.RemoveAt(0);
		}
	}


    // Update is called once per frame
    void Update()
    {
		StartNextMovement();
    }
}
