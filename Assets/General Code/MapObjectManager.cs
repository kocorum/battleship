using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class MapObjectManager : MonoBehaviour
{
	// Assets
	public GameObject earth;
	public GameObject water;
	public GameObject fog;
	public GameObject building;
	public GameObject fire;

	// Config
	public bool isEnemy;
	public GameObject mapObject;

	// Useful
	public Matrix<GameObject> Grid;

	public void Initialize(PlayMap playMap)
	{
		Grid = new Matrix<GameObject>(playMap.Terrain.Size);
		if (isEnemy)
		{
			mapObject.transform.position += new Vector3(0, playMap.Terrain.Height * 1.5f, 0);
		}

		foreach (var terrainCell in playMap.Terrain.ToList())
		{
			GameObject obj = null;
			if (!isEnemy)
			{
				obj = terrainCell.Value == TerrainType.Ground ? earth : water;
				if (playMap.Buildings.SelectMany(z => z.BuildingCells).Any(z => z.MasterPosition == terrainCell.Position))
					obj = building;
			}
			else
			{
				obj = fog;
			}

			var cube = Instantiate(obj, mapObject.transform);
			cube.transform.localPosition = TranslatePosition(terrainCell.Position);
			cube.name = terrainCell.Position.ToString();
			Grid[terrainCell.Position] = cube;
		}
	}

	/// <summary>
	/// Returns the Unity position of a Matrix position (relative to its parent)
	/// </summary>
	public Vector3 TranslatePosition(Position position)
	{
		return new Vector3(position.X, position.Y * -1, 0);
	}
}