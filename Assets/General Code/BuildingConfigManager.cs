using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static Heatmap;

public static class BuildingConfigManager
{
	public static bool ConfigMatchesTerrain(BuildingConfigCell buildingCell, TerrainType? terrain, bool isOutOfBounds)
	{
		return ConfigMatchesTerrain(buildingCell, TerrainHeatmapCell.GetTerrainValue(terrain), isOutOfBounds) == 1f;
	}
	public static float ConfigMatchesTerrain(BuildingConfigCell buildingCell, float? waterProbability, bool isOutOfBounds)
	{
		if (isOutOfBounds)
			return (!buildingCell.IsPartOfBuilding
				&& buildingCell.LandPlacement != CellPlacementPossibility.Required
				&& buildingCell.WaterPlacement != CellPlacementPossibility.Required)
				? 1f : 0f;

		var validity = 1f;

		if (waterProbability.HasValue)
		{
			if (buildingCell.LandPlacement == CellPlacementPossibility.Impossible)
				validity = Mathf.Min(validity, waterProbability.Value);
			else if (buildingCell.LandPlacement == CellPlacementPossibility.Required)
				validity = Mathf.Min(validity, 1 - waterProbability.Value);

			if (buildingCell.WaterPlacement == CellPlacementPossibility.Impossible)
				validity = Mathf.Min(validity, 1 - waterProbability.Value);
			else if (buildingCell.WaterPlacement == CellPlacementPossibility.Required)
				validity = Mathf.Min(validity, waterProbability.Value);
		}

		return validity;
	}

	public static List<BuildingConfig> GetAllBuildingConfigs()
	{
		var result = new List<BuildingConfig>();
		{
			// 1 cell
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "Dr. Delta's cabin";
			newConfig.Description = "Dr. Delta is a crazy inventor who can live anywhere. Every once in a while, he creates a random weapon ready to use.";
			newConfig.CellsAsString = "111";
			newConfig.NumberOfBuildingCells = 1;
			result.Add(newConfig);
		}
		{
			// 2 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "Swift Alice";
			newConfig.Description = "A small boat that can only be placed on water. Can move along the river's path.";
			newConfig.CellsAsString = "102.102";
			newConfig.NumberOfBuildingCells = 2;
			result.Add(newConfig);
		}
		{
			// 2 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "Twin Towers";
			newConfig.Description = "The most iconic duo. Two inseparable brothers. If one falls down, the other one will rebuild it next turn.";
			newConfig.CellsAsString = "120.120";
			newConfig.NumberOfBuildingCells = 2;
			result.Add(newConfig);
		}
		{
			// 3 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "Silver Bridge";
			newConfig.Description = "A shiny bridge that goes over water. Although it is not yellow, it still helps the city develop faster.";
			newConfig.CellsAsString = "120.102.120";
			newConfig.NumberOfBuildingCells = 3;
			result.Add(newConfig);
		}
		{
			// 3 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "TNT warehouse";
			newConfig.Description = "A simple storage unit for all your city's TNT. Can enlarge your missiles' explosion radius. Careful: If attacked, the explosions will blast off through its doors!";
			newConfig.CellsAsString = "120.120.120/020.020.020";
			newConfig.NumberOfBuildingCells = 3;
			result.Add(newConfig);
		}
		{
			// 3 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "Drone Headquarters";
			newConfig.Description = "A garage from which we can send and receive drones that specialize in spying.";
			newConfig.CellsAsString = "120.120/120.011";
			newConfig.NumberOfBuildingCells = 3;
			result.Add(newConfig);
		}
		{
			// 4 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "The Last Resort";
			newConfig.Description = "A bunker that must be placed far away from water. Can resist any attack as long as its entrance is intact.";
			newConfig.CellsAsString = "020.020.020.020/020.120.120.020/020.120.120.020/020.020.020.020";
			newConfig.NumberOfBuildingCells = 4;
			result.Add(newConfig);
		}
		{
			// 4 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "Earth's hammer";
			newConfig.Description = "A giant pounding device made to create earthquakes. Needs a water source nearby, but can weaken the enemy's largest standing building.";
			newConfig.CellsAsString = "120.120.120/011.120.011/011.002.011";
			newConfig.NumberOfBuildingCells = 4;
			result.Add(newConfig);
		}
		{
			// 4 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "Weird construction site";
			newConfig.Description = "No one really knows what this place is supposed to be, but there are a lot of building materials that can be used to create dummy structures.";
			newConfig.CellsAsString = "120.011/120.120/011.120";
			newConfig.NumberOfBuildingCells = 4;
			result.Add(newConfig);
		}
		{
			// 4 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "Area 52";
			newConfig.Description = "A secret base where alien technology is being investigated. Lets you know the identity of the buildings you hit before destroying them.";
			newConfig.CellsAsString = "120.120.120/011.011.120";
			newConfig.NumberOfBuildingCells = 4;
			result.Add(newConfig);
		}
		{
			// 5 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "L.A.S.E.R.";
			newConfig.Description = "A laboratory that also serves as a giant laser designed for war. Stands for 'Long Attack Services for Emergency Retaliation'.";
			newConfig.CellsAsString = "120.120.120.120.120";
			newConfig.NumberOfBuildingCells = 5;
			result.Add(newConfig);
		}
		{
			// 5 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "Protection centre";
			newConfig.Description = "A structure that specializes in generating shields for its city.";
			newConfig.CellsAsString = "120.120.120/011.120.011/011.120.011";
			newConfig.NumberOfBuildingCells = 5;
			result.Add(newConfig);
		}
		{
			// 5 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "Future's tower";
			newConfig.Description = "The most technologically advanced edifice of its time. Rumors say a time-travel technology has been created here.";
			newConfig.CellsAsString = "011.120.011/120.120.120/011.120.011";
			newConfig.NumberOfBuildingCells = 5;
			result.Add(newConfig);
		}
		{
			// 5 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "Project U";
			newConfig.Description = "The secret project aiming to manufacture undetectable nuclear bombs.";
			newConfig.CellsAsString = "120.120.120/120.020.120";
			newConfig.NumberOfBuildingCells = 5;
			result.Add(newConfig);
		}
		{
			// 6 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "Nanobots container";
			newConfig.Description = "The nanobots have been contained, but if this building is breached, they will have to be released on enemy territory.";
			newConfig.CellsAsString = "120.120.120/120.120.120";
			newConfig.NumberOfBuildingCells = 6;
			result.Add(newConfig);
		}
		{
			// 6 cells
			var newConfig = ScriptableObject.CreateInstance<BuildingConfig>();
			newConfig.BuildingName = "Water pump";
			newConfig.Description = "A pump so powerful, it can send the flow of water to your enemy's river, and vice-versa. The perfect tool for inundations and droughts.";
			newConfig.CellsAsString = "000.000.000.000.000/120.120.002.120.120/120.120.002.120.120";
			newConfig.NumberOfBuildingCells = 6;
			result.Add(newConfig);
		}
		// Aim for a total of 20 cells


		return result;
	}

	public static List<BuildingConfig> SelectRandomBuildingsForSetup(int seed)
	{
		var result = new List<BuildingConfig>();

		var availableBuildings = GetAllBuildingConfigs().OrderBy(z => Random.value).ToList();

		var randomOriginalState = Random.state;
		try
		{
			Random.InitState(seed);

			while (result.Count < StaticSettings.GameSetup.MinBuildingCountOnMap || result.Sum(z => z.NumberOfBuildingCells) < StaticSettings.GameSetup.MinBuildingCellsOnMap)
			{
				foreach (var potentialBuilding in availableBuildings.ToList())
				{
					availableBuildings.Remove(potentialBuilding);
					var newRemainingBuildingCount = StaticSettings.GameSetup.MaxBuildingCountOnMap - 1 - result.Count;
					var newRemainingBuildingCellCount = StaticSettings.GameSetup.MaxBuildingCellsOnMap - potentialBuilding.NumberOfBuildingCells - result.Sum(z => z.NumberOfBuildingCells);

					// If the building would break the limit
					if (newRemainingBuildingCellCount < 0 || newRemainingBuildingCount < 0)
					{
						availableBuildings.Remove(potentialBuilding);
						continue;
					}
					var minimumRemainingBuildingCellCount = availableBuildings.OrderBy(z => z.NumberOfBuildingCells).Take(newRemainingBuildingCount).Sum(z => z.NumberOfBuildingCells);
					var maximumRemainingBuildingCellCount = availableBuildings.OrderByDescending(z => z.NumberOfBuildingCells).Take(newRemainingBuildingCount).Sum(z => z.NumberOfBuildingCells);
					// If we place a building too small or too big that we wont be able to reach the target cell numbers
					if (newRemainingBuildingCellCount > maximumRemainingBuildingCellCount || newRemainingBuildingCellCount < minimumRemainingBuildingCellCount)
					{
						availableBuildings.Remove(potentialBuilding);
						continue;
					}
					result.Add(potentialBuilding);
				}

				// If we reached the maximum number of buildings on the map
				if (result.Count == StaticSettings.GameSetup.MaxBuildingCountOnMap || !availableBuildings.Any())
					break;
			}

			if (result.Count < StaticSettings.GameSetup.MinBuildingCountOnMap)
				throw new AIBuildingPlacementException($"Didn't reach the minimum building count. Minimum: {StaticSettings.GameSetup.MinBuildingCountOnMap} Actual: {result.Count}");

			if (result.Count > StaticSettings.GameSetup.MaxBuildingCountOnMap)
				throw new AIBuildingPlacementException($"Exceeded the maximum building count. Maximum: {StaticSettings.GameSetup.MaxBuildingCountOnMap} Actual: {result.Count}");

			if (result.Sum(z => z.NumberOfBuildingCells) < StaticSettings.GameSetup.MinBuildingCellsOnMap)
				throw new AIBuildingPlacementException($"Didn't reach the minimum building cells count. Minimum: {StaticSettings.GameSetup.MinBuildingCellsOnMap} Actual: {result.Sum(z => z.NumberOfBuildingCells)}");

			if (result.Sum(z => z.NumberOfBuildingCells) > StaticSettings.GameSetup.MaxBuildingCellsOnMap)
				throw new AIBuildingPlacementException($"Exceeded the maximum building cells count. Maximum: {StaticSettings.GameSetup.MaxBuildingCellsOnMap} Actual: {result.Sum(z => z.NumberOfBuildingCells)}");
		}
		finally
		{
			Random.state = randomOriginalState;
		}

		return result;
	}
}
