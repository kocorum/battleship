using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameSetupManager : MonoBehaviour
{
    public MapGenerationService mapService;
    public MapCameraManager mapCamera;
    public MapObjectManager playerMapObjectManager;

    // Start is called before the first frame update
    void Start()
    {
        var map = mapService.CreateMapWithRiver(StaticSettings.GameSetup.MapWidth, StaticSettings.GameSetup.MapHeight, 0);
        playerMapObjectManager.Initialize(map);


        //ArtificialIntelligence.RunAITest(map);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
