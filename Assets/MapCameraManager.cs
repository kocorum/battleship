using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// At the moment, the camera manager queues up every movement requested, not giving the option to cancel them. 
/// </summary>
public class MapCameraManager : CameraManager
{
	public GameObject defaultMap;
	public GameObject playerMap;
	public GameObject enemyMap;
	public bool? viewingEnemyMap = null;

	private void Start()
	{
		if (playerMap != null && enemyMap != null)
		{
			ViewBothMaps(true);

			if (defaultMap?.name == playerMap?.name)
			{
				ViewPlayerMap(false);
			}
			if (defaultMap?.name == enemyMap?.name)
			{
				ViewEnemyMap(false);
			}
		}
		else
		{
			if (playerMap != null)
				ViewPlayerMap(true);
			else if (enemyMap != null)
				ViewEnemyMap(true);
		}
	}

	private Vector3 GetTargetCameraPositionFor(params GameObject[] maps)
	{
		var bounds = GetTotalBounds(maps);
		return new Vector3(bounds.center.x, bounds.center.y, ZDistanceThatFitsBounds(bounds));
	}
	/// <summary>
	/// TODO: Change this method when we'll want to do smooth movement instead of linear
	/// </summary>
	private float GetMovingCameraAnimation(bool instant)
	{
		return instant ? 0f : 0.75f;
	}

	public void ViewPlayerMap(bool instant, Action callback = null)
	{
		viewingEnemyMap = false;
		MoveTo(GetTargetCameraPositionFor(playerMap), GetMovingCameraAnimation(instant), callback);
	}

	public void ViewEnemyMap(bool instant, Action callback = null)
	{
		viewingEnemyMap = true;
		MoveTo(GetTargetCameraPositionFor(enemyMap), GetMovingCameraAnimation(instant), callback);
	}
	public void ViewBothMaps(bool instant, Action callback = null)
	{
		viewingEnemyMap = null;
		MoveTo(GetTargetCameraPositionFor(playerMap, enemyMap), GetMovingCameraAnimation(instant), callback);
	}

	public void ToggleMap(bool instant, Action callback = null)
	{
		if (viewingEnemyMap != false)
			ViewPlayerMap(instant, callback);
		else
			ViewEnemyMap(instant, callback);
	}

	public void DefaultToggleMap()
	{
		ToggleMap(false);
	}

}
