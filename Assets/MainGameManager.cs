using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MainGameManager : MonoBehaviour
{
	public MapGenerationService mapService;
	public MapCameraManager mapCamera;
	public MapObjectManager playerMapObjectManager;
	public MapObjectManager enemyMapObjectManager;

	public PlayMap PlayerMap { get; set; }
	public PlayMap EnemyMap { get; set; }

	private FakePlayer Bot;

	// Start is called before the first frame update
	void Start()
	{
		InitializeBot();

		// TODO: Use the map from the GameSetup scene. Right now this is a new map, for the purposes of testing
		PlayerMap = mapService.CreateMapWithRiver(StaticSettings.GameSetup.MapWidth, StaticSettings.GameSetup.MapHeight, Random.Range(0, int.MaxValue));
		var tempBot = new FakePlayer(AIDifficulty.Normal, Random.Range(0, int.MaxValue))
		{
			Map = PlayerMap
		};

		// Randomly assign buildings and place them
		tempBot.MyBuildingConfigs = BuildingConfigManager.SelectRandomBuildingsForSetup(tempBot.Seed);
		tempBot.PlaceBuildings();

		playerMapObjectManager.Initialize(PlayerMap);

		Diagnostics.LogAll();
	}

	public void InitializeBot()
	{
		// Create bot and its map

		EnemyMap = mapService.CreateMapWithRiver(StaticSettings.GameSetup.MapWidth, StaticSettings.GameSetup.MapHeight, Random.Range(0, int.MaxValue));
		Bot = new FakePlayer(AIDifficulty.Normal, Random.Range(0, int.MaxValue))
		{
			Map = EnemyMap
		};

		// Randomly assign buildings and place them
		Bot.MyBuildingConfigs = BuildingConfigManager.SelectRandomBuildingsForSetup(Bot.Seed);
		Bot.PlaceBuildings();
		enemyMapObjectManager.Initialize(EnemyMap);
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown(KeyCode.Space))
		{
			mapCamera.DefaultToggleMap();
		}
	}
}
